/*
 * org.nrg.dcm.CopyOp
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm;

import org.apache.commons.io.FileUtils;
import org.nrg.transaction.OperationI;
import org.nrg.transaction.RollbackException;
import org.nrg.transaction.Transaction;
import org.nrg.transaction.TransactionException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class CopyOp extends Transaction {
	final File rootBackup;            //the main directory into which files or directories are backed up
	final LoggerI logger;        
	
	final Map<String,File> dirs;      // an associative list of files
	final Map<String,File> backups;   // an associative list of backups
	final OperationI<Map<String,File>> op;

	
	public CopyOp(OperationI<Map<String,File>> op, File rootBackup, Map<String,File> dirs) { 
		this(op, rootBackup, dirs, new LoggerI() {
			public void info(String l) {}
			public void failed (String l) {} 
		});
	}
	
	/**
	 * The default constructor
	 * @param op         The operation to run on a set of files or directories
	 * @param rootBackup The directory into which to backup the files or directories
	 * @param dirs       A map of names to Files so they can be retrieved by the user's code.
	 * @param logger     The logger to use.
	 */
	public CopyOp(OperationI<Map<String,File>> op, File rootBackup, Map<String,File> dirs, LoggerI logger) { 
		this.dirs = dirs;
		this.op = op;
		this.rootBackup = rootBackup;
		this.logger = logger;
		Map<String,File> backups = new HashMap<String,File>();
		for (String f : dirs.keySet()) {
			backups.put(f, null);
		}
		this.backups = backups;
	}
	
	public CopyOp(OperationI<Map<String,File>> op, File rootBackup, File dir) {
		this(op, rootBackup, CopyOp.addToMap(dir));
	}
	
	public CopyOp(OperationI<Map<String,File>> op, File rootBackup, Set<File> dirs) {
		this(op, rootBackup, CopyOp.addToMap(dirs));
	}
	
	private static Map<String,File> addToMap(File dir) {
		Map<String,File> fs = new HashMap<String,File>();
		fs.put("0", dir);
		return fs;
	}
	
	private static Map<String,File> addToMap(Set<File> dirs) {
		Integer counter = 0;
		Map<String,File> fs = new HashMap<String,File>();
		for (File f : dirs) {
			fs.put(counter.toString(), f);
			counter++;
		}
		return fs;
	}
	
    private File copy(File f) throws TransactionException {
		rootBackup.mkdirs();
		rootBackup.setWritable(true);
		
		logger.info("Backing up source directory");
		try {
			if (f.isDirectory()) {
				FileUtils.copyDirectoryToDirectory(f, rootBackup);
			}
			else {
				FileUtils.copyFile(f, rootBackup);
			}
		} catch (Exception e) {
			logger.failed("Failed to backup source directory");
			throw new TransactionException (e.getMessage(),e);
		}
		return new File(rootBackup, f.getName());
	}
	
	
	public void run() throws TransactionException {
		for (String f : this.backups.keySet()) {
			if (this.dirs.get(f) != null && this.dirs.get(f).exists()) {
				this.backups.put(f, this.copy(this.dirs.get(f)));	
			}
		}
		try {
			this.op.run(dirs);
			for (File f : this.backups.values()) {
				if (f.isFile()) {
					f.delete();
				}
				else if (f.isDirectory()) {
					FileUtils.deleteDirectory(f);
				}
			}
			if (rootBackup.isDirectory()) {
				if (rootBackup.list().length == 0) {
					FileUtils.deleteDirectory(rootBackup);
				}
			}
		}
		catch (Throwable e){
			throw new TransactionException(e.getMessage(),e);
		}
	}

    public void rollback() throws RollbackException {
		try {
			File backupTmp = new File(rootBackup, "modified_dest");
			backupTmp.mkdirs();
			for (String f : backups.keySet()){
				if (this.dirs.get(f) != null) {
					String originalPath = this.dirs.get(f).getAbsolutePath();
					File b = backups.get(f);
					if (! b.renameTo(new File(backupTmp, b.getName()))) {
						throw new RollbackException("unable to rename " + b.getAbsolutePath() + " to " + new File(backupTmp, b.getName()).getAbsolutePath());
					}
					else {
						try {
							FileUtils.deleteDirectory(this.dirs.get(f));
						}
						catch (IOException e) {
							throw new RollbackException(e);
						}
					}
					File newB = new File(backupTmp, b.getName());
					if (! newB.renameTo(new File(originalPath))) {
						throw new RollbackException("unable to rename " + newB.getAbsolutePath() + " to " + originalPath);
					}	
				}
			}
		}
		catch (RollbackException e) {
			logger.failed("Failed to restore previous version of destination directory.");
			throw e;
		}
	}
}
