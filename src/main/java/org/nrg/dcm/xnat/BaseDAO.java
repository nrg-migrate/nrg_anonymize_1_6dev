/*
 * org.nrg.dcm.xnat.BaseDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/5/13 1:10 PM
 */
package org.nrg.dcm.xnat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.framework.orm.hibernate.BaseHibernateEntity;
import org.springframework.transaction.annotation.Transactional;

public class BaseDAO<T extends BaseHibernateEntity> extends AbstractHibernateDAO<T> {
	
	protected BaseDAO() {
		super();
	}
	/**
	 * Get the most recent row associated with the given project
	 * @param project
	 * @return
	 */
	@Transactional
	public T getMostRecentRow(Long project) {
		return this.getMostRecentRow(this.getByProject(project));
	
	}
	@Transactional
	public T getOldestRow(List<T> all) {
		List<T> tmp = this.sortRows(all);
		return tmp.size() == 0 ? null : tmp.get(0);
	}
	@Transactional
	public T getMostRecentRow(List<T> all) {
		List<T> tmp = this.sortRows(all);
		return tmp.size() == 0 ? null : tmp.get(tmp.size() - 1);
	}
	
	/**
	 * Sort the given list of rows into ascending order by the row id.
	 * @param all
	 * @return
	 */
	public List<T> sortRows(List<T> all) {
		List<T> tmp = new ArrayList<T>();
		tmp.addAll(all);
		Collections.sort(tmp, new Comparator<T>(){
			public int compare(T t1, T t2) {
				return ((Long) t1.getId()).compareTo((Long) t2.getId());
			}
		});
		return tmp;
	}
		
	/**
	 * Get all the entities where the given column is null.
	 * @param column
	 * @return
	 */
	public List<T> getAllNull (String column) {
		Criteria criteria = getCriteriaForType();
        criteria.add(Restrictions.eq("enabled", true));
        criteria.add(Restrictions.isNull(column));
        @SuppressWarnings("rawtypes")
        List list = criteria.list();
        
        if (list == null) {
        	return new ArrayList<T>();
        }
        else {
        	List<T> ret = new ArrayList<T>();
        	ret.addAll(list);
        	return ret;
        }
	}
	
	/**
	 * Rows cannot be deleted from the Script table or the Edit table
	 */
	public void delete (T t) {
		throw new UnsupportedOperationException("Cannot delete from the Script or the Edit table.");
	}
	
	/**
	 * Retrieve all the rows with with the given project.
	 * @param projectId    The ID of the project to search.
	 * @return All objects of the parameterized type that are associated with the indicated project.
	 */
	@Transactional
	public List<T> getByProject(Long projectId) {
        Criteria criteria = getCriteriaForType();
        criteria.add(Restrictions.eq("enabled", true));
        if (null == projectId) {
        	criteria.add(Restrictions.isNull("projectId"));
        }
        else {
        	criteria.add(Restrictions.eq("projectId", projectId));
        }
        
        @SuppressWarnings("rawtypes")
        List list = criteria.list();
        
        if (list == null) {
        	return new ArrayList<T>();
        }
        else {
        	List<T> ret = new ArrayList<T>();
        	ret.addAll(list);
        	return ret;
        }
    }
	
	/**
	 * Retrieve the entire table.
	 * @return
	 */
	public List<T> getAll() {
		Criteria criteria = getCriteriaForType();
		@SuppressWarnings("rawtypes")
		List list = criteria.list();
		List<T> ret = new ArrayList<T>();
		ret.addAll(list);
		return ret;
	}
}
