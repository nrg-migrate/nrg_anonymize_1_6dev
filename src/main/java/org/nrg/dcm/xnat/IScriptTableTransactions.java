/*
 * org.nrg.dcm.xnat.IScriptTableTransactions
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm.xnat;

import java.util.List;

import org.nrg.dcm.ScriptTableException;

public interface IScriptTableTransactions {
	/**
	 * Scan the script table and ensure that invariants are met.
	 * @return
	 */
	public List<ScriptTableException> findProblems();
	/**
	 * Return the ScriptTable objects associated with this project. 
	 *  
	 * @param project XNAT project id or null to retrieve all versions of the site-wide ScriptTable object 
	 * @return ScriptTable object or null if not found
	 */
	public List<ScriptTable> getByProject(String project);
	
	/**
	 * Get everything in the persistent store
	 * @return All rows in the persistent store 
	 */
	public List<ScriptTable> getAll();
	
	/**
	 * Get this project's most recent anonymization script.	
	 * @param project XNAT project id or null to get retrieve the latest site-wide script.
	 * @return ScriptTable object or null if not found
	 */
	public ScriptTable getMostRecentRow(String project);
	
	/**
	 * Synonym for this.getLatestScript(project)
	 * @param project XNAT project id or null to retrieve the latest site-wide script.
	 * @return ScriptTable object or null if not found
	 */
	public ScriptTable get(String project);
	
	/**
	 * Add a script to this project.
	 * @param project XNAT project id
	 * @param script Script contents
	 */
	public void insertScript(String project, String script, String user) throws ScriptTableException;
}
