/*
 * org.nrg.dcm.Anonymize
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 11/13/13 9:40 AM
 */
package org.nrg.dcm;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.io.DicomCodingException;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.dcm4che2.iod.module.composite.PatientModule;
import org.dcm4che2.iod.module.macro.Code;
import org.nrg.dcm.edit.AttributeException;
import org.nrg.dcm.edit.ScriptApplicator;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.Variable;
import org.nrg.transaction.RollbackException;
import org.nrg.transaction.Run;
import org.nrg.transaction.TransactionException;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
public final class Anonymize {
	// default code values 
	/**
	 * The tag which contains the history of edit script application. 
	 * (0012,0064) - Deidentification Method Code Sequence is a sequence tag meaning that 
	 * it can contain multiple nested DICOM objects.
	 */
	public final static int RecordTag = Tag.DeidentificationMethodCodeSequence;
	
	/**
	 * The following are the tags that comprise each history entry in the sequence 
	 * tag above.
	 * These along with a Code Value tag, conform to the minimal required for a Code 
	 * Entry as specified by the DICOM standard.
	 * TODO: Provide a link to the specific part of the standard  
	 */
	public final static String Meaning = "XNAT Edit Script";
	public final static String Designator = "XNAT";
	public final static String Version = "0.1";
	
	/**
	 * The DICOM standard and DCM4CHE differ on the max. allowable lengths of the tags described above.
	 * In the DCM4CHE implementation all the tags are set to 8 bytes (64 chars) which is why
	 * all values written to the history tag are checked for length against the DICOM standard conforming 
	 * lengths below. 
	 */
	public final static int MeaningLength = 64;
	public final static int VersionLength = 16;
	public final static int DesignatorLength = 16;
	public final static int ValueLength = 16;
	
	/**
	 *
     * Anonymize the dicom file by first applying the default script and
     * then the project specific one.
     * 
     * All exceptions throws by this function should be considered fatal. 
     * 
     * The caller should then make sure to delete the dicom file 
     * from the filesystem.
     * 
	 * @param f The DICOM file
	 * @param project The project to which this file belongs
	 * @param subject The subject to which this file belongs
	 * @param session The session to which this file belongs
	 * @param defaultScript The default site-wide script
	 * @param projectScript The project specific script
	 * @throws IOException
	 */
    @Deprecated
    public final static void anonymize(final File f, 
    		                           final String project, 
    		                           final String subject, 
    		                           final String session , 
    		                           final File defaultScript, 
    		                           final File projectScript) throws IOException {
    	if(defaultScript!=null && defaultScript.exists() && defaultScript.length()>0){
    	Anonymize.anonymize(f, project, subject, session, false, null ,defaultScript);
    	}
    	if (projectScript != null) {
    		Anonymize.anonymize(f, project, subject, session, false, null, projectScript);	
    	}
    }
    
    
  /**
   	*
    * Anonymize the dicom file by applying the given edit script.
    * 
    * All exceptions throws by this function should be considered fatal. 
    * 
    * The caller should then make sure to delete the dicom file 
    * from the filesystem.
    * 
    * NOTE: The record and scriptId arguments indicate whether to record the application of this 
    * script in the DICOM header and what the ID of the script is. For that reason if "record" is 
    * false, the scriptId isn't checked and allowed to be null. If "record" is true, the "scriptId" 
    * cannot be null and results in a runtime exception. 
    * 
    * This method which accepts the script as a file (and not a string from the database) so most of the intended usage will 
    * not require recording to the DICOM header but the functionality is here just in case. 
    * 
    * This is really janky, but Java doesn't have pattern-matching on tuples and wrapping "record" and 
    * "scriptId" into an object makes this function more opaque and harder to use. 
    *  
    * 
	* @param dicomFile The DICOM file
	* @param project The project to which this file belongs
	* @param subject The subject to which this file belongs
	* @param session The session to which this file belongs
	* @param record A boolean indicating whether to record the application of these scripts in the DICOM header
	* @param scriptId The database id of this script. Only required if the "record" flag is true.
	* @param script The script to be applied to the given DICOM file
	* @throws IOException
	* @throws IllegalArgumentException
	*/
    @Deprecated
    public final static void anonymize(final File dicomFile, 
    		 						   final String project, 
    		 						   final String subject, 
    		 						   final String session,
    		 						   final boolean record,
    		 						   final Long scriptId,
    		 						   final File script) throws IOException, 
    		 						   						     IllegalArgumentException {
    	Anonymize.anonymize(dicomFile, project, subject, session, record, scriptId, new FileInputStream(script));
    }

    
  /**
   	*
    * Anonymize the dicom file by applying the given edit script.
    * 
    * All exceptions throws by this function should be considered fatal. 
    * 
    * The caller should then make sure to delete the dicom file 
    * from the filesystem.
    * 
    * NOTE: The record and scriptId arguments indicate whether to record the application of this 
    * script in the DICOM header and what the ID of the script is. For that reason if "record" is 
    * false, the scriptId isn't checked and allowed to be null. If "record" is true, the "scriptId" 
    * cannot be null and results in a runtime exception. 
    * 
    * This method which accepts the script as a file (and not a string from the database) so most of the intended usage will 
    * not require recording to the DICOM header but the functionality is here just in case. 
    * 
    * This is really janky, but Java doesn't have pattern-matching on tuples and wrapping "record" and 
    * "scriptId" into an object makes this function more opaque and harder to use. 
    *  
    * 
	* @param dicomFile The DICOM file
	* @param project The project to which this file belongs
	* @param subject The subject to which this file belongs
	* @param session The session to which this file belongs
	* @param record A boolean indicating whether to record the application of these scripts in the DICOM header
	* @param scriptId The database id of this script. Only required if the "record" flag is true.
	* @param script The script to be applied to the given DICOM file
	* @throws IOException
	* @throws IllegalArgumentException
	*/
    @Deprecated
    public final static void anonymize(final File dicomFile, 
    								   final String project, 
    								   final String subject, 
    								   final String session,
    								   final boolean record,
    								   final Long scriptId,
    								   final String script) throws IOException, 
    								                             IllegalArgumentException {
    	Anonymize.anonymize(dicomFile, project, subject, session, record, scriptId, new ByteArrayInputStream(script.getBytes("UTF-8")));
    }
    /**
     * Add a record indicating that the application of the script with the given id was 
     * applied to the DICOM file to the DICOM header. 
     * @param o
     * @param uniqueId
     * @return A DicomObject with the a record of anonymization
     * @throws NullPointerException
     * @throws Exception
     */
    public final static DicomObject addRecord(DicomObject o, long uniqueId) throws NullPointerException, Exception {
    	return Anonymize.addRecord(o, 
    			                   uniqueId,
    			                   Anonymize.Meaning,
    			                   Anonymize.Designator,
    			                   Anonymize.Version);
    }
    
    /**
     * Ensure that the given code tags are within the size limits specified by the DICOM standard.
     * @param uniqueId
     * @param meaning
     * @param schemeDesignator
     * @param schemeVersion
     * @return
     */
    private final static boolean validateSizes(String uniqueId, String meaning, String schemeDesignator, String schemeVersion) {
    	return  uniqueId.length() <= Anonymize.ValueLength && 
				schemeDesignator.length() <= Anonymize.DesignatorLength &&
				meaning.length() <= Anonymize.MeaningLength &&
				schemeVersion.length() <= Anonymize.VersionLength;
    }
    
    
    /**
     * Add a record to the DICOM header of the given DICOM object indicating that it has been edited.
     * @param o
     * @param uniqueId
     * @param meaning
     * @param schemeDesignator
     * @param schemeVersion
     * @return A DicomObject with a with a record of anonymization
     * @throws NullPointerException
     * @throws Exception
     */
    public final static DicomObject addRecord(DicomObject o, 
    										  long uniqueId, 	
    										  String meaning, 
    										  String schemeDesignator, 
    										  String schemeVersion) throws NullPointerException, Exception {
    	
    	if (uniqueId < Integer.MIN_VALUE || uniqueId > Integer.MAX_VALUE) {
    		throw new Exception("The unique id " + uniqueId + " should be between " + Integer.MIN_VALUE + " and " + Integer.MAX_VALUE);
    	}
    	String s_uniqueId = Long.toString(uniqueId);
    	if (!validateSizes(s_uniqueId, meaning, schemeDesignator, schemeVersion)) {
    		throw new Exception("One of the arguments: " + s_uniqueId + "," 
    				                                     + meaning + "," 
    				                                     + schemeDesignator + "," 
    				                                     + schemeVersion + " " 
    				                                     + "exceeds the size fixed by the DICOM standard.");
    	}
    	DicomObject recorded = new BasicDicomObject();
    	o.copyTo(recorded);
    	PatientModule p = new PatientModule(recorded);
    	if (recorded.get(Anonymize.RecordTag) == null) {
    		recorded.putSequence(Anonymize.RecordTag);
    	} 
    	DicomElement record = recorded.get(Anonymize.RecordTag);
    	List<Code> cs = new ArrayList<Code>(Arrays.asList(Code.toCodes(record)));
    	Code c = new Code(new BasicDicomObject());
    	c.setCodeMeaning(meaning);
    	c.setCodeValue(((Long)uniqueId).toString());
    	c.setCodingSchemeDesignator(schemeDesignator);
    	c.setCodingSchemeVersion(schemeVersion);
    	cs.add(c);
    	p.setDeidentificationMethodCodes(cs.toArray(new Code[cs.size()]));
    	return p.getDicomObject();
    }
    
    public static StopTagInputHandler defaultInputHandler() {
    	return new StopTagInputHandler(Anonymize.RecordTag + 1);
    }
    
    public static Code[] getCodes (File f) throws IOException {
    	DicomInputStream dis = new DicomInputStream(f);
    	dis.setHandler(defaultInputHandler());
    	DicomObject o = dis.readDicomObject();
        dis.close();
        dis = null;
    	return Anonymize.getCodes(o);
    }
    
    public static Code[] getCodes (DicomObject o) {
    	DicomElement record = o.get(Anonymize.RecordTag);
    	Code[] c = Code.toCodes(record);
    	return c;
    }
    
    /**
     * Apply the given script to the given dicom file on the filesystem.
     * From a PHI point-of-view all exceptions throws by this function 
     * should be considered fatal. 
     * 
     * The dicom data is read from the given file, but the unchanged pixel data and 
     * changed headers are written to a temporary file. The given file is replaced with the
     * temporary file if the anonymization process is successful.
     * 
     * NOTE: The record and scriptId arguments indicate whether to record the application of this 
     * script in the DICOM header and what the ID of the script is. For that reason if "record" is 
     * false, the scriptId isn't checked and allowed to be null. If "record" is true, the "scriptId" 
     * cannot be null and results in a runtime exception. 
     * 
     * This is really janky, but Java doesn't have pattern-matching on tuples and wrapping "record" and 
     * "scriptId" into an object makes this function more opaque and harder to use.
     **/
    static class AnonymizeHelper extends CallOnFile<java.lang.Void> {
    	final File dicomFile;
		final String project;		
		final String subject; 
		final String session; 
		final boolean record;
		final Long scriptId;
		final InputStream script;
    	AnonymizeHelper (final File dicomFile, 
    					 final String project,
    					 final String subject, 
    					 final String session, 
    					 final boolean record,
    					 final Long scriptId,
    					 final InputStream script) {
    		this.dicomFile = dicomFile;
    		this.project = project;
    		this.subject = subject;
    		this.session = session;
    		this.record = record;
    		this.scriptId = scriptId;
    		this.script = script;
    	}	
    	
    	// The dicom data is read from the given file, but the unchanged pixel data and 
    	// changed headers are written to a temporary file in the system's temp directory under
    	// in the "anon_backup" directory. The given file is replaced with the
    	// temporary file if the anonymization process is successful. The "anon_backup" directory
    	// is left in place.    	
		@Override
		public Void call() throws Exception {
			IOException ioexception = null;			
	    	ScriptApplicator s = new ScriptApplicator (new BufferedInputStream (script));
	    	
	    	// Give the values in this session to the variables in the script.
	    	setVariable(s,"project", project);
	    	setVariable(s,"subject", subject);
	    	setVariable(s,"session", session);
	    	    	    	
	    	FileInputStream dicom_fis = null;
            BufferedInputStream bis = null;
            DicomInputStream dis = null;
            FileOutputStream dicom_out = null;
            DicomOutputStream dos = null;
            try {
                dicom_fis = new FileInputStream(dicomFile);
                bis = new BufferedInputStream(dicom_fis);
                dis = new DicomInputStream(bis);
                dicom_out = new FileOutputStream(this.getFile().getAbsolutePath());

                try {
                    final StopTagInputHandler handler = getStopTagInputHandler(s);
                    if (null != handler) {
                        dis.setHandler(handler);
                    }
	    			DicomObject o = dis.readDicomObject();

                    s.apply(dicomFile,o);
	    			if (record) {
	    				if (scriptId == null) {
	    					throw new IllegalArgumentException("\"record\" is true, but \"scriptId\" is null.");
	    				}
	    				else {
	    					o = Anonymize.addRecord(o, scriptId);	
	    				}
	    			}
	    			dos = new DicomOutputStream(dicom_out);
	                dos.setAutoFinish(false);
	                final String tsuid = o.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian);
	                if (o.contains(Tag.FileMetaInformationVersion)) {
	                    dos.writeFileMetaInformation(o);
	                } else {
	                    final DicomObject fmi = new BasicDicomObject();
	                    fmi.initFileMetaInformation(o.getString(Tag.SOPClassUID), o.getString(Tag.SOPInstanceUID), tsuid);
	                    dos.writeFileMetaInformation(fmi);
	                }
	                dos.writeDataset(o, tsuid);
	                bis.reset();
	                ByteStreams.copy(bis, dicom_out);
                }
	    		catch (IOException e) {
	    			throw ioexception = e;
	    		}
	     		finally {
	    			try {
                        if (dis != null) {
                            dis.close();
                        }
                        if (bis != null) {
                            bis.close();
                        }
                        if (dicom_fis != null) {
                            dicom_fis.close();
                        }
                        dis = null;
                        bis = null;
                        dicom_fis = null;

                        if (dos != null) {
                            dos.flush();
                            dos.close();
                        }
                        if (dicom_out != null) {
                            dicom_out.flush();
                            dicom_out.close();
                        }
                        dos = null;
                        dicom_out = null;
	    			}
	    			catch (IOException e) {
	    				throw ioexception = (null == ioexception) ? e : ioexception;
	    			}
	    		}
	    	}
	    	catch (DicomCodingException e) {
	    		throw ioexception = (null == ioexception) ? new IOException(e) : ioexception;
	    	}
            return null;
		}
    }
    
    
    /**	
     * Apply the script to the DICOM header.  
     * @param dicomFile The given dicom file on the filesystem
     * @param project Project name
     * @param subject Subject name
     * @param session Session name
     * @param record Boolean indicating if this edit be recorded in the DICOM header. 
     * @param scriptId Unique identifier of the script in the database. Only necessary if the "record" flag is true.
     * @param script The anonymization script
     * 
     * @throws IOException
     * @throws IllegalArgumentException
     * 
     */
    @Deprecated
    public final static void anonymize(final File dicomFile, 
    								   final String project,
    								   final String subject, 
    								   final String session, 
    								   final boolean record,
    								   final Long scriptId,
    								   final InputStream script) throws IOException,
    								   									IllegalArgumentException {
    	
    	AnonymizeHelper _h = new AnonymizeHelper(dicomFile,project,subject,session,record,scriptId,script);
    	File tmpdir = new File (System.getProperty("java.io.tmpdir"), "anon_backup");
    	try {
    		WorkOnCopyOp anonymizeOp = new WorkOnCopyOp(dicomFile,tmpdir,_h);
			Run.runTransaction(anonymizeOp);
		} catch (RollbackException e) {
			throw new IOException (e.getCause());
		} catch (TransactionException e) {
			throw new IOException(e.getCause());
		}
	}
    
    private static final StopTagInputHandler getStopTagInputHandler(final ScriptApplicator applicator) {
        // Scanning Sequence is the largest internally required tag:
        // > SOP Class UID and all of File Metainformation Header
        final long ssqtag = 0xffffffffL & Tag.ScanningSequence;
        final long atop = 0xffffffffL & applicator.getTopTag();
        if (0xffffffffL == atop) {
            return null;
        } else if (atop > ssqtag) {
            return new StopTagInputHandler((int)(atop + 1));
        } else {
            return new StopTagInputHandler(Tag.ScanningSequence+1);
        }
    }
    
    /**
     * Set a variable in the script
     * @param s
     * @param name
     * @param value
     * @return
     */
    private final static boolean setVariable(ScriptApplicator s, String name, String value) {
    	boolean set = false;
    	Map<String,Variable> m = s.getVariables();
    	Variable v = m.get(name);
    	if (null != v) {
    		v.setValue(value);
    		set = true;
    	}
    	return set;
    }
    
    /**
     * Anonymize the given dicom object.
     *  
     * @param d The given dicom file on the filesystem
     * @param project Project name
     * @param subject Subject name
     * @param session Session name
     * @param script The anonymization script
     * @throws IOException
     * @throws AttributeException
     * @throws ScriptEvaluationException
     * @throws FileNotFoundException
     */
    @Deprecated
    public final static void anonymize(DicomObject d, 
    								   String project, 
    								   String subject, 
    								   String session, 
    								   File script) throws FileNotFoundException, 
    								   					   IOException, 
    								   					   ScriptEvaluationException, 
    								   					   AttributeException {
    	ScriptApplicator s = new ScriptApplicator (new FileInputStream (script));
    	
    	// Give the values in this session to the variables in the script.
    	setVariable(s,"project", project);
    	setVariable(s,"subject", subject);
    	setVariable(s,"session", session);
    	s.apply(null,d);
    }
    
    /**
     * Anonymize the given dicom object in place.
     *  
     * @param d The Dicom OBject
     * @param project Project name
     * @param subject Subject name
     * @param session Session name
     * @param script The anonymization script
     * @throws IOException
     * @throws AttributeException
     * @throws ScriptEvaluationException
     * @throws FileNotFoundException
     */
    @Deprecated
    public final static void anonymize(DicomObject d, 
    								   String project, 
    								   String subject, 
    								   String session, 
    								   String script) throws FileNotFoundException, 
    								   					   IOException, 
    								   					   ScriptEvaluationException, 
    								   					   AttributeException {
    	ScriptApplicator s = new ScriptApplicator (new ByteArrayInputStream(script.getBytes("UTF-8")));
    	
    	// Give the values in this session to the variables in the script.
    	setVariable(s,"project", project);
    	setVariable(s,"subject", subject);
    	setVariable(s,"session", session);
    	s.apply(null,d);
    }

    public final static void anonymize(DicomObject d,
                                       String project,
                                       String subject,
                                       String session,
                                       ScriptApplicator s) throws FileNotFoundException,
            IOException,
            ScriptEvaluationException,
            AttributeException {
        // Give the values in this session to the variables in the script.
        setVariable(s,"project", project);
        setVariable(s,"subject", subject);
        setVariable(s,"session", session);
        s.apply(null,d);
    }
    
    /**
     * Determine if two anon scripts are equals by reading the text of the script into
     * memory and comparing. 
     * 
     * @param oldScript
     * @param newScript
     * @return Are the two scripts equal in content?
     * @throws IOException
     */
    public final static boolean needAnonymization(File oldScript, File newScript) throws IOException {
    	boolean out = false;
    	List<String> o = Files.readLines(oldScript, Charset.defaultCharset());
    	List<String> n = Files.readLines(newScript, Charset.defaultCharset());
    	if (o.size() == n.size()) {
    		Iterator<String> oi = o.iterator();
        	Iterator<String> ni = n.iterator();
        	while (oi.hasNext() && ni.hasNext()) {
        		String _o = oi.next();
        		String _n = ni.next();
        		out = out || !_o.equals(_n); 
        	}
    	}
    	else {
    		out = true;
    	}
    	return out;
    }
}
