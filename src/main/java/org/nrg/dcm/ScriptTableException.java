/*
 * org.nrg.dcm.ScriptTableException
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/13 6:15 PM
 */
package org.nrg.dcm;

import java.util.ArrayList;
import java.util.List;

import org.nrg.dcm.xnat.ScriptTable;

public class ScriptTableException extends Throwable {
	
	public final List<ScriptTable> problem_scripts = new ArrayList<ScriptTable>();

	public ScriptTableException() {
	}
	
	public ScriptTableException(String message) {
		super(message);
	}

	public ScriptTableException(Throwable cause) {
		super(cause);
	}

	public ScriptTableException(String message, Throwable cause){
		super(message, cause);
	}

	public ScriptTableException(String message, List<ScriptTable> ss) {
		super(message);
		this.problem_scripts.addAll(ss);
	}

	public ScriptTableException(Throwable cause, List<ScriptTable> ss) {
		super(cause);
		this.problem_scripts.addAll(ss);
	}

	public ScriptTableException(String message, Throwable cause, List<ScriptTable> ss) {
		super(message, cause);
		this.problem_scripts.addAll(ss);
	}
}
